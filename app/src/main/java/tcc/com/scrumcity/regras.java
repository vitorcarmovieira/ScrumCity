package tcc.com.scrumcity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class regras extends AppCompatActivity {

    private TextView tvRegras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regras);

        String texto = getResources().getString(R.string.regras_label);

        tvRegras = (TextView) findViewById(R.id.tv_regras);
        tvRegras.setText(Html.fromHtml(texto));
    }

    private void iniciarConstrucao() {
        Intent i = new Intent(regras.this, BuildActivity.class);

        String[] a = {"casa", "arvore", "garagem", "banco"};

        i.putExtra("pages", a);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_regras, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_start:
                this.iniciarConstrucao();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
