package tcc.com.scrumcity;

import android.content.ClipData;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

/**
 * Created by vitorvieira on 10/27/16.
 */

public class FirstFragment extends Fragment {
    // Store instance variables
    public String title;
    private int page;

    // newInstance constructor for creating fragment with arguments
    public static FirstFragment newInstance(int page, String title) {
        FirstFragment fragmentFirst = new FirstFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page = getArguments().getInt("someInt", 0);
        title = getArguments().getString("someTitle");

    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_first, container, false);

        LinearLayout ll = (LinearLayout) view.findViewById(R.id.LLFragment1);

        ll.setOnDragListener(new MyDragListener());

        int cont = 1;
        int id = getResources().getIdentifier(title+String.valueOf(cont), "drawable", getContext().getPackageName());
        Drawable image = null;
        try {
            image = ContextCompat.getDrawable(getContext(), id);
        }catch (Resources.NotFoundException e){
            image = null;
        }

        while(image!=null){

            ImageView iv = new ImageView(getContext());
            iv.setOnTouchListener(new MyTouchListener());
            iv.setImageDrawable(image);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(100, 100);
            lp.setMargins(10, 10, 10, 10);
            iv.setLayoutParams(lp);
            iv.setMaxHeight(20);
            iv.setMaxWidth(20);


            ll.addView(iv);

            cont++;
            id = getResources().getIdentifier(title+String.valueOf(cont), "drawable", getContext().getPackageName());
            try {
                image = ContextCompat.getDrawable(getContext(), id);
            }catch (Resources.NotFoundException e){
                image = null;
            }
        }



        return view;
    }



    private final class MyTouchListener implements View.OnTouchListener {
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                ClipData data = ClipData.newPlainText("", "");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
                        view);
                view.startDrag(data, shadowBuilder, view, 0);
                view.setVisibility(View.INVISIBLE);
                return true;
            } else {
                return false;
            }
        }
    }

    class MyDragListener implements View.OnDragListener {

        int resource0 = R.drawable.shape;
        int resource1 = R.drawable.shape_itens;
        int resource0_target = R.drawable.shape_droptarget;
        int resource1_target = R.drawable.shape_itens_droptarget;

        @Override
        public boolean onDrag(View v, DragEvent event) {
            int action = event.getAction();
            switch (action) {
                case DragEvent.ACTION_DRAG_STARTED:
                    // do nothing
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:

                    if (v.getId() == R.id.LLFragment1) {
                        v.setBackgroundResource(resource1_target);
                    } else if( v.getId() == R.id.topleft) {
                        v.setBackgroundResource(resource0_target);
                    }

                    break;
                case DragEvent.ACTION_DRAG_EXITED:

                    if (v.getId() == R.id.LLFragment1) {
                        v.setBackgroundResource(resource1);
                    } else if( v.getId() == R.id.topleft) {
                        v.setBackgroundResource(resource0);
                    }

                    break;
                case DragEvent.ACTION_DROP:
                    // Dropped, reassign View to ViewGroup
                    View view = (View) event.getLocalState();
                    ViewGroup owner = (ViewGroup) view.getParent();
                    owner.removeView(view);
                    LinearLayout container = (LinearLayout) v;
                    container.addView(view);
                    System.out.println(container);
                    view.setVisibility(View.VISIBLE);

                    break;
                case DragEvent.ACTION_DRAG_ENDED:

                    if (v.getId() == R.id.LLFragment1) {
                        v.setBackgroundResource(resource1);
                    } else if( v.getId() == R.id.topleft) {
                        v.setBackgroundResource(resource0);
                    }

                default:
                    break;
            }
            return true;
        }

    }
}