package tcc.com.scrumcity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ComecarEntrevistaActivity extends AppCompatActivity {

    Button avancar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comecar_entrevista);

        this.avancar = (Button) findViewById(R.id.avancar2);

        this.avancar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ComecarEntrevistaActivity.this, QuizActivity.class);
                startActivity(i);
            }
        });
    }
}
