package tcc.com.scrumcity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class BemVindoActivity extends AppCompatActivity {

    Button avancar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bem_vindo);

        this.avancar = (Button) findViewById(R.id.avancar1);

        this.avancar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(BemVindoActivity.this, ComecarEntrevistaActivity.class);
                startActivity(i);
            }
        });

    }
}
