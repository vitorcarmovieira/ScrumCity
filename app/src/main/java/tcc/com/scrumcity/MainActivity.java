package tcc.com.scrumcity;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import quiz.Answer;
import quiz.EduGame;
import quiz.GameHolder;
import quiz.Question;
import quiz.QuestionDatabase;
import quiz.Session;
import quiz.TriviaGame;

public class MainActivity extends AppCompatActivity {

    Button btSobre;
    Button btJogar;

    TextView tvNomeUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.btSobre = (Button) findViewById(R.id.sobre);
        this.btJogar = (Button) findViewById(R.id.jogar);

        this.btSobre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, SobreActivity.class);
                startActivity(i);
            }
        });

        this.btJogar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, BemVindoActivity.class);
                startActivity(i);
            }
        });
















        QuestionDatabase qd = QuestionDatabase.getInstance();
        List<Question> questions = new LinkedList<Question>();

        List<String> categories = new ArrayList<>();
        categories.add("scrum");


        String[] questionsText = getResources().getStringArray(R.array.questions);

        for (int i=0; i<questionsText.length; i++){


            String nameId = "answers"+String.valueOf(i);
            int resId = getResources().getIdentifier(nameId, "array", getPackageName());
            String[] answersText = getResources().getStringArray(resId);

            List<Answer> answers = new ArrayList<>();
            boolean rightAnswer = true;
            for (String answer : answersText){

                answers.add(new Answer(answer, rightAnswer));

                rightAnswer = false;
            }

            String[] explaText = getResources().getStringArray(R.array.explanations);

            Question q = new Question(questionsText[i], answers, categories);
            q.setExplanation(explaText[i]);
            questions.add(q);

        }


















        qd.prepare(questions);


        SessionUtils.setSession(this, new Session());
        TriviaGame game = new TriviaGame(QuestionDatabase.getInstance(),new ArrayList<String>());
//        EduGame game = new EduGame(QuestionDatabase.getInstance(), new ArrayList<String>());
        GameHolder.setInstance(game);





    }
}
