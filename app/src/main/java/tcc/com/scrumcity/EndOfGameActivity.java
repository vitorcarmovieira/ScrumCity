package tcc.com.scrumcity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import quiz.*;

public class EndOfGameActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_end_of_game);
//		ActionBarHelper.setUpActionBar(this);

		showStats();
		
		((Button)findViewById(R.id.restart)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				restart();
				
			}
		});
	}
	
	public void showStats() {
		Game game = GameHolder.getInstance();
		if (game!=null) {
			Session session = game.getSession();
			((TextView)findViewById(R.id.totalAnswers)).setText("Total de respostas: "+session.getTotalAttempts());
			((TextView)findViewById(R.id.correctAnswers)).setText("Respostas corretas: "+session.getCorrectAttempts());
			((TextView)findViewById(R.id.correctAnswersRate)).setText("Taxa de acerto: "+session.getCorrectnessRate());
			((TextView)findViewById(R.id.bestSequence)).setText("Melhor sequência: "+session.getBestConsecutiveAttempts());
			((TextView)findViewById(R.id.overallRecord)).setText("Recorde: "+game.getRecord());
		}
	}
	
	public void restart() {
		GameHolder.getInstance().reset();
		SessionUtils.setSession(this, new Session());		
		finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.l, menu);
		return true;
	}

}
