package tcc.com.scrumcity;

import android.content.ClipData;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.astuetz.PagerSlidingTabStrip;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;


public class BuildActivity extends AppCompatActivity {
    /** Called when the activity is first created. */

    private ViewPager viewPager;
    private PagerSlidingTabStrip tabStrip;
    FragmentPagerAdapter adapterViewPager;

    String[] pages;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_build);
//        findViewById(R.id.myimage1).setOnTouchListener(new MyTouchListener());
//        findViewById(R.id.myimage2).setOnTouchListener(new MyTouchListener());
//        findViewById(R.id.myimage3).setOnTouchListener(new MyTouchListener());
//        findViewById(R.id.myimage4).setOnTouchListener(new MyTouchListener());
        findViewById(R.id.topleft).setOnDragListener(new MyDragListener());
//        findViewById(R.id.topright).setOnDragListener(new MyDragListener());
//        findViewById(R.id.bottomleft).setOnDragListener(new MyDragListener());
//        findViewById(R.id.bottomright).setOnDragListener(new MyDragListener());

        pages = getIntent().getStringArrayExtra("pages");
//        pages = new ArrayList<>();

        this.adapterViewPager = new MyPagerAdapter(getSupportFragmentManager(), pages);
        this.viewPager = (ViewPager) findViewById(R.id.viewpager);
        this.viewPager.setAdapter(adapterViewPager);

        this.tabStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        this.tabStrip.setViewPager(viewPager);
        this.markSelectedTab(0, 0);

    }

    private int markSelectedTab(int currentPageSelected, int position){
        Field field = null;
        try {
            field = PagerSlidingTabStrip.class.getDeclaredField("tabsContainer");
            field.setAccessible(true);
            LinearLayout tabsContainer = (LinearLayout) field.get(BuildActivity.this.tabStrip);
            tabsContainer.getChildAt(currentPageSelected).setSelected(false);
            currentPageSelected = position;
            tabsContainer.getChildAt(position).setSelected(true);

        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return currentPageSelected;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_next) {

            Intent i = new Intent(this, BuildActivity.class);
            String[] a = {"casa", "arvore", "garagem", "banco"};
            i.putExtra("pages", a);
            startActivity(i);


        }else if (id == R.id.action_info){

        }

        return super.onOptionsItemSelected(item);
    }



    private final class MyTouchListener implements OnTouchListener {
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                ClipData data = ClipData.newPlainText("", "");
                DragShadowBuilder shadowBuilder = new DragShadowBuilder(
                        view);
                view.startDrag(data, shadowBuilder, view, 0);
                view.setVisibility(View.INVISIBLE);
                return true;
            } else {
                return false;
            }
        }
    }

    class MyDragListener implements OnDragListener {

        int resource0 = R.drawable.shape;
        int resource1 = R.drawable.shape_itens;
        int resource0_target = R.drawable.shape_droptarget;
        int resource1_target = R.drawable.shape_itens_droptarget;

        @Override
        public boolean onDrag(View v, DragEvent event) {
            int action = event.getAction();
            switch (action) {
                case DragEvent.ACTION_DRAG_STARTED:
                    // do nothing
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:

                    if (v.getId() == R.id.LLFragment1) {
                        v.setBackgroundResource(resource1_target);
                    } else if( v.getId() == R.id.topleft) {
                        v.setBackgroundResource(resource0_target);
                    }

                    break;
                case DragEvent.ACTION_DRAG_EXITED:

                    if (v.getId() == R.id.LLFragment1) {
                        v.setBackgroundResource(resource1);
                    } else if( v.getId() == R.id.topleft) {
                        v.setBackgroundResource(resource0);
                    }

                    break;
                case DragEvent.ACTION_DROP:
                    // Dropped, reassign View to ViewGroup
                    View view = (View) event.getLocalState();
                    ViewGroup owner = (ViewGroup) view.getParent();
                    owner.removeView(view);
                    LinearLayout container = (LinearLayout) v;
                    container.addView(view);
                    System.out.println(container);
                    view.setVisibility(View.VISIBLE);

                    break;
                case DragEvent.ACTION_DRAG_ENDED:

                    if (v.getId() == R.id.LLFragment1) {
                        v.setBackgroundResource(resource1);
                    } else if( v.getId() == R.id.topleft) {
                        v.setBackgroundResource(resource0);
                    }

                default:
                    break;
            }
            return true;
        }
    }









    public static class MyPagerAdapter extends FragmentPagerAdapter implements PagerSlidingTabStrip.IconTabProvider {
        private static int NUM_ITEMS = 4;

        String[] pages;
        private final int[] ICONS = { R.mipmap.ic_home, R.mipmap.ic_tree, R.mipmap.ic_garage, R.mipmap.ic_chair};

        public MyPagerAdapter(FragmentManager fragmentManager, String[] names) {
            super(fragmentManager);
            pages = names;
        }

        // Returns total number of pages
        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        // Returns the fragment to display for that page
        @Override
        public FirstFragment getItem(int position) {
            switch (position) {
                case 0: // Fragment # 0 - This will show FirstFragment
                    return FirstFragment.newInstance(0, "casa");
                case 1: // Fragment # 0 - This will show FirstFragment
                    return FirstFragment.newInstance(1, "arvore");
                case 2: // Fragment # 0 - This will show FirstFragment
                    return FirstFragment.newInstance(2, "garagem");
                case 3: // Fragment # 0 - This will show FirstFragment
                    return FirstFragment.newInstance(3, "banco");
                default:
                    return null;
            }
        }

        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {
            return pages[position];
        }

        @Override
        public int getPageIconResId(int position) {
            return ICONS[position];
        }
    }



}
