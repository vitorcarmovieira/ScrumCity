package tcc.com.scrumcity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class ParabenizacaoActivity extends AppCompatActivity {

    private TextView tvInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parabenizacao);

        String label = getResources().getString(R.string.parabenizacao_label);
        this.tvInfo = (TextView) findViewById(R.id.parabenizacao_label);
        this.tvInfo.setText(Html.fromHtml(label));

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_parabenizacao, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_terminar:

                //TODO something

                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

}
